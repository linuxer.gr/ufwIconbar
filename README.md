# Preparing this script for Artix Repos

# ufwIconbar  (Add an icon with UFW status to the panel)

![Screenshot](https://imgur.com/osOZxnR.png)

- Show an icon into panel when UFW (Uncomplicated FireWall) is actived or deactivated.

- Tested with XFCE4 Desktop environment (Artix, Arch & Manjaro) 

- Requisites:  yad

- forked from https://github.com/juanmafont/ufwIconbar
